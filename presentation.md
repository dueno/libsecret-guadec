---
title: Desktop secrets management for the future
author: Daiki Ueno
date: 2019-08-23
header-includes:
  - \usepackage[sfdefault]{FiraSans}
  - \usepackage{tikz}
  - \usetikzlibrary{positioning}
  - \usetikzlibrary{shapes}
---

## Agenda

- Objective
- Solutions: past, present, and future
- Discussion

\note{
First I will define the problem to solve. Secondly, will quickly go
through the past attempts and plans for the future. Finally, will move
on to the discussion section where I would like to hear your opinions
and suggestions as the project is still ongoing.
}

## Objective

Allow applications to **store user credentials** in a uniform way

### User credentials = some attributes + secret
- Sign-in forms on websites
- Access tokens for web services
- Wi-Fi passwords
- Protection passwords for SSH keys

\note{
The objective is to allow applications to store user credentials in a
uniform way.

Here "user credentials" means a secret associated with some
attributes. A typical example is a sign-in forms on the web, where URL
and username are attributes and the password is a secret.

They are actully not limited to the web.  Wi-Fi passwords are
associated with networks, and SSH passwords are associated with a file
name.
}

# Solutions

\note{}

## Past: `~/.netrc`

Invented for FTP clients, adopted by other applications

- User credentials are in plain text with matching attributes
  ```
  machine foo login ueno password baz
  machine bar port 587 login ueno password baz
  default login anonymous password user@site
  ```

### Potential attackers
- Other users on the system

### Protection
- File permissions

\note{
In 1970-1980, there was a well-known file called .netrc, widely used
in legacy applications.  The mechanism was primarily invented for FTP
clients, because at that time FTP was more common than HTTP.

The file lists one credential per line. Each line consists of a set of
matching attributes followed by login name and the password.

This is so simple and only protected by file permissions, because at
that time, computer users were typically sharing a single computer, so
the potential attackers were other users on the system.
}

## Present: gnome-keyring and libsecret

The central daemon manages all the user credentials

- Applications are supposed to use a client library to access

### Potential attackers
- Other users on the system
- Active attackers who gain access to the disk

### Protection
- File permissions
- Encryption

\note{
In 1990-2000, gnome-keyring was born. It is a D-Bus service that
centrally manages user credentials. The applications are supposed to
use a dedicated library to access.

At this time, it is common that everyone has their own computer at
home. That means we need to worry about a different type of attacks.
That is an active attacker who gains access to the data.

On the server side, the credentials are protected with
encryption.
}

## Present: gnome-keyring and libsecret

![](gnome-keyring-and-libsecret.pdf)

\note{
Here is the diagram showing the architecture.
}

## Future: is the current technology still relevant?

- Per-process isolation
- Keyring format

\note{
So now that we are in 2019, the question is whether the current
technology is still good enough. I see a couple of issues in the
current architecture and will show you how we could improve the
situation.
}

## Per-process isolation

gnome-keyring doesn't provide isolation between processes

- One app can retrieve the other app's credentials
- flatpak makes installing third party apps easier than ever
- flatpak apps need a **hole** to access the Secret Service API
  ```javascript
 /* Secret Service API */
  "--talk-name=org.freedesktop.secrets"
  ```

### Potential attackers
- Other users on the system
- Active attackers who gain access to the disk
- Malicious third party apps

\note{
Per-process isolation. gnome-keyring actually doesn't provide any
isolation between processes. So if one application has an access to
gnome-keyring, it is also possible to access the other applications'
secrets.

This is not a problem if users are using trusted applications
only. But after flatpak has been introduced, it is becoming more
common to install third party applications.

On the other hand, to allow access to gnome-keyring, you need to allow
applications to fully access the gnome-keyring D-Bus service.

So now we have another type of potential attackers, which is a
malicious third party apps.
}

## Can we close the hole?

![](hole.jpg)


\note{}

## Can we close the hole?

Solution: Let's store credentials in application side

- Analogous to GSettings: ["Settings, in a sandbox world"](https://blogs.gnome.org/mclasen/2019/07/12/settings-in-a-sandbox-world/)
- Master secrets are still needed for encryption

Approaches

- Kernel keyring upcall
- Portal and FD passing

\note{
The simple mitigation of this problem is to store the credentials in
application side, not on the host side, as GSettings does recently.

However, the situation is a bit different to GSettings.  That is, the
applications still need master secrets to encrypt the credentials.

There are a couple of approaches.  One is using kernel keyring and the
other is adding a portal.
}

## Retrieving master secret: kernel keyring upcall

1. Look up kernel keyring for master secret
2. If not found, access gnome-keyring through upcall
3. Cache the retrieved secret in kernel keyring

\note{
Kernel keyring upcall. This was first introduced by Stef Walter in
GUADEC 2013, utilizing keyring mechanism in Linux kernel.
}

## Retrieving master secret: kernel keyring upcall

![Kernel keyring upcall](keyring-upcall.pdf)

\note{
Here is the diagram showing the architecture.
}

## Retrieving master secret: kernel keyring upcall

### Pros
- Master passwords are cached securely in the kernel
- No need for wire encryption

### Cons
- The callout program lacks app information
- Impossible to cancel prompting if keyring is locked
- Linux only

\note{}

## Retrieving master secret: portal and FD passing

1. Ask flatpak portal for master secret
2. Portal redirects the request to gnome-keyring
3. Secrets are transported through FD passing

\note{}

## Retrieving master secret: portal and FD passing

![Portal and FD passing](portal-and-fd-passing.pdf)

\note{
Here is the diagram showing the architecture.
}

## Retrieving master secret: portal and FD passing

### Pros
- No need for wire encryption
- Ability to cancel prompting by application
- Portable

### Cons
- Applications have more responsibility

\note{}

## Future: is the current technology still relevant?

- Per-process isolation
- Keyring format

\note{}

## Keyring format

::: {.columns}

:::: {.column}

- Custom binary format
- **Items are encrypted in a single chunk**
- SHA-256 for key derivation
- AES-128-CBC for encryption
- **MD5 for hashing attributes**

::::

:::: {.column width=60%}

\begin{tikzpicture}
\node[rectangle split, rectangle split parts=4, rectangle split part fill={blue!0,blue!0,blue!0,green!50}, draw, align=center, text width=2cm] (encrypted)
{header\\
\nodepart{two}
metadata\\
\nodepart{three}
hashed attributes
\nodepart{four}
encrypted items
\vspace*{2cm}
};

\node[rectangle split, rectangle split parts=7, rectangle split part fill={green!50}, draw, align=center, text width=2cm, right=1.5cm of encrypted] (protected)
{label\\
secret\\
attributes\\};

\node [above] at (encrypted.north) {Keyring};
\node [above] at (protected.north) {Items};

\draw[->] (encrypted) to node [above,font=\footnotesize] {decrypt} (protected);

\end{tikzpicture}

::::

:::

\note{
The current keyring format looks like this. This format has been
unchanged for a while.  I think the choices of the algorithms are
quite sensible, but there are a couple of drawbacks.

The one is that items are encrypted as a single chunk with locked
memory, and the locked memory has a limit.

The other is that attributes of each item are hashed for later
indexing, but it is merely MD5 hashes.
}

## Keyring format

```sh
$ strings ~/.local/share/keyrings/login.keyring
GnomeKeyring
Login
host
 5f4a67b0504e75a1b659441bfdddd948
keyring
 42a31547b300ad0c1123774ec91f1a9b
unique
 8ae8c34a73360d83a5be7bbe68e930cd

$ echo -n ssh-store:/home/dueno/.ssh/sakura_rsa | md5sum
8ae8c34a73360d83a5be7bbe68e930cd  -
```

\note{}

## Proposed keyring format

::: {.columns}

:::: {.column}

- GVariant serialization format
- Items are encrypted individually
- PBKDF2
- AES-256-CBC
- MAC instead of simple hashing

::::

:::: {.column width=60%}

\begin{tikzpicture}
\node[rectangle split, rectangle split parts=9, rectangle split part fill={blue!0,green!10,green!50,green!10,green!50,green!10,green!50,green!10,green!50}, draw, align=center, text width=2.3cm] (encrypted)
{header\\
\nodepart{two}
keyed hash of attributes
\nodepart{three}
encrypted items
\nodepart{four}
\phantom{a}
\nodepart{five}
\phantom{a}
\nodepart{six}
\phantom{a}
\nodepart{seven}
\phantom{a}
\nodepart{eight}
\phantom{a}
};

\node[rectangle, fill={green!50}, draw, align=center, text width=2cm, right=1.5cm of encrypted, yshift=1.5cm] (protected)
{label\\
secret\\
attributes\\};

\node[rectangle, fill={green!50}, draw, align=center, text width=2cm, below=5mm of protected] (protected1)
{\phantom{a}};

\node[rectangle, fill={green!50}, draw, align=center, text width=2cm, below=5mm of protected1] (protected2)
{\phantom{a}};

\node[rectangle, fill={green!50}, draw, align=center, text width=2cm, below=5mm of protected2] (protected3)
{\phantom{a}};

\node [above] at (encrypted.north) {Keyring};
\node [above] at (protected.north) {Items};

\draw[->] (encrypted) to node [above,font=\footnotesize] {decrypt} (protected);
\draw[-] (encrypted) to node [above,font=\footnotesize] {} (protected1);
\draw[-] (encrypted) to node [above,font=\footnotesize] {} (protected2);
\draw[-] (encrypted) to node [above,font=\footnotesize] {} (protected3);

\end{tikzpicture}

::::

:::

\note{
This is the new format I am proposing, and actually using in the local
credentials.

Overall, it uses a GVariant serialization format. Items are encrypted
individually, and attributes of each item are hashed with a keyed hash
algorithm.
}

## Current status

Demo

- [user credentials isolation under flatpak](https://asciinema.org/a/9gAd1DMQuucd3brCGp3SynBtj)

4 open merge requests

- [libsecret: Add local-storage backend](https://gitlab.gnome.org/GNOME/libsecret/merge_requests/6)
- [libsecret: New interface to represent storage backend](https://gitlab.gnome.org/GNOME/libsecret/merge_requests/34)
- [xdg-desktop-portal: Add a secret portal](https://github.com/flatpak/xdg-desktop-portal/pull/359)
- [gnome-keyring: Implement secret portal backend](https://gitlab.gnome.org/GNOME/gnome-keyring/merge_requests/18)

\note{
Here is the current status. I created a little video.

The implementation is currently ongoing. There are 4 merge requests
that are still open so I would appreciate any reviews.
}

# Discussion

\note{}

## Discussion

What happens if app ID is forged after reinstall?

- Remove app's keyring file on uninstall, or
- Ensure all apps are digitally signed

Can we close the hole entirely?

- Anonymize the host access using public key crypto
- TLS exporters, Clevis / Tang
- Need a physical boundary between the sandbox and host

\note{}

## Discussion

How about backup and mobility of secrets?

- Not feasible to store arbitrary credentials on HSM
- Only store a key pair used to encrypt master secrets
- Share the secrets on remote machine, as an encrypted file

\note{}

## Thanks!

Questions, comments or suggestions?

\vspace*{5cm}

\footnotesize
Image credit: [view from a hole](https://flic.kr/p/4evCaN) by spDuchamp, CC-BY-SA 2.0

\note{}
