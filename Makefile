NULL =

SRC = guadec-example.tex
PANDOC_SRC = presentation.md

OUT = $(SRC:%.tex=%.pdf)
PANDOC_OUT = $(PANDOC_SRC:%.md=%.pdf)

BYPRODUCTS = \
	$(SRC:%.tex=%.aux) \
	$(SRC:%.tex=%.log) \
	$(SRC:%.tex=%.nav) \
	$(SRC:%.tex=%.out) \
	$(SRC:%.tex=%.snm) \
	$(SRC:%.tex=%.toc) \
	$(NULL)

FIGURES = \
	gnome-keyring-and-libsecret.pdf \
	keyring-upcall.pdf \
	portal-and-fd-passing.pdf \
	$(NULL)

default: pandoc pandoc-notes

ctex: tex clean
tex: $(OUT)

pandoc: $(PANDOC_SRC) $(FIGURES)
	@echo -n "Compiling $(PANDOC_SRC) to $(PANDOC_OUT)... "
	@pandoc $(PANDOC_SRC) -t beamer+smart -s -o $(PANDOC_OUT) \
	-V theme:guadec -V classoption:aspectratio=43 --slide-level 2 --highlight-style kate --pdf-engine=xelatex
	@echo "Done!"

pandoc-notes: $(PANDOC_SRC) $(FIGURES)
	@echo -n "Compiling $(PANDOC_SRC) to $(basename $(PANDOC_OUT))-notes.pdf... "
	@pandoc $(PANDOC_SRC) -t beamer+smart -s -o $(basename $(PANDOC_OUT))-notes.pdf \
	-V theme:guadec -V classoption:aspectratio=43 --slide-level 2 --highlight-style kate --pdf-engine=xelatex -H preamble-notes.tex
	@echo "Done!"

continuous: pandoc
	@echo "The PDF will be updated automatically when you save the $(PANDOC_SRC) document. Press Ctrl+C to abort."
	@while inotifywait -q $(PANDOC_SRC); do sleep 0.1; make --no-print-directory pandoc; done

%.pdf: %.tex
	pdflatex $<

gnome-keyring-and-libsecret.pdf: stencils.odg
	@echo -n "Extracting $@ from $<... "
	@unoconv -e PageRange=1-1 -f pdf -o $@ $<
	@echo "Done!"

keyring-upcall.pdf: stencils.odg
	@echo -n "Extracting $@ from $<... "
	@unoconv -e PageRange=2-2 -f pdf -o $@ $<
	@echo "Done!"

portal-and-fd-passing.pdf: stencils.odg
	@echo -n "Extracting $@ from $<... "
	@unoconv -e PageRange=3-3 -f pdf -o $@ $<
	@echo "Done!"

clean:
	rm -Rf $(OUT) $(PANDOC_OUT) $(BYPRODUCTS) $(FIGURES)

.PHONY: all clean

